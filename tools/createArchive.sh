#!/bin/bash

mkdir /tmp/Coilhac_Jumel
cd /tmp/Coilhac_Jumel
wget https://formation-nsi.frama.io/projet-algo-du/documents/Dossier_Eleve.pdf
wget https://formation-nsi.frama.io/projet-algo-du/documents/Dossier_Prof.pdf
wget https://framagit.org/formation-nsi/projet-algo-du/-/archive/v0.2.4/projet-algo-du-v0.2.4.zip
wget https://formation-nsi.frama.io/projet-algo-du/graphe_initial.png
wget https://formation-nsi.frama.io/projet-algo-du/graphe_petit.png
mkdir prez
cd prez
wget https://formation-nsi.frama.io/projet-algo-du/prez/index.html
for i in Accroche_cahier_charges.md Algorithme_pseudocode_paradigme.md \
Attendus_eleves.md But_pedagogique.md Conditions_matérielles.md PlanningProjet.md \
Prerequis.md definition-projet.md prerequis_prof.md ressources.md ;
do
    wget https://formation-nsi.frama.io/projet-algo-du/prez/$i ;
done ;
cd ..
cd ..
zip -r Coilhac_Jumel.zip Coilhac_Jumel/
rm -r /tmp/Coilhac_Jumel
