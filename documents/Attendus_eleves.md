### Évaluation

#### Attendus des élèves


  1. Faire un exposé avec présentation, et démonstration de l'exécution du programme.

  2. Rédiger un dossier contenant :

        - le cahier des charges
        - un planning des différentes tâches exécutées par chacun
        - une explication des problèmes rencontrés
        - des copies d'écran des résultats obtenus
        - le code complet du programme en annexe

note: Mireille

Le dossier comporte au moins 10 pages hors annexes

---

#### Jalons

  - Présentation du cahier des charges (respect d'un format imposé, présence de tests, …)
  - Évaluation par les pairs dans le groupe de travail : chaque élève évalue de façon
  confidentielle auprès du professeur l'implication de ses camarades dans le projet
  - Respect des dates des jalons
  - Le rendu final répond au cahier des charges 

---

#### Compétences travaillées

  - collaborer avec des camardes
  - manipuler des graphes
  - parcourir un graphe en largeur ou en profondeur
  - utiliser un dictionnaire

---

#### Grille d'évaluation

|  Critère                                       | Niveau (MI, MF, BM, TBM)|
| :--------------------------------------------- | ----------------------: |
|  Respect du cahier des charges                 |                         |
|  Le code est correctement exécutable           |                         |
|  Les tests sont respectés et validés           |                         |
|  Les fonctions sont documentées                |                         |

---

|  Critère                                       | Niveau (MI, MF, BM, TBM)|
| :--------------------------------------------- | ----------------------: |
|  Toutes les fonctions sont testées             |                         |
|  Les problèmes sont identifiés par des tickets |                         |
|  Les dates des jalons ont été respectées       |                         |

---

|  Critère                                       | Niveau (MI, MF, BM, TBM)|
| :--------------------------------------------- | ----------------------: |
|  Avis des camarades sur l'implication          |                         |
|  Avis de l'enseignant sur l'implication        |                         |

