### Accroche et cahier des charges

#### Situation problème

  - une classe de 27 élèves
  - un professeur qui désire des groupes de 3

note: Vx

La classe de première NSI est composée de 27 élèves qui doivent réaliser un projet informatique par groupes de 3. Vous devez réaliser un programme qui fasse une proposition de la composition de ces groupes de 3 élèves, qui leur convienne le mieux possible .


---

#### Le travail déjà réalisé

  - un élève avec qui il voudrait absolument travailler (affecté de la valeur +2),
  - un avec qui il voudrait travailler (affecté de la valeur +1)
  - et un avec qui il ne voudrait pas travailler (affecté de la valeur -1)


note: Pour vous aider, le professeur a réalisé une petite enquête : il a demandé à chaque élève de citer:

Ces données vont être fournies par le professeur sous la forme d’un dictionnaire :

---

#### Le format retenu
```python
Classe_NSI={ 'Alexia': {'Kellian': 2, 'Christelle': 1,
  'Alice': -1}, ...}
```

note: Ici Alexia voudrait absolument travailler avec Kellian, voudrait bien
travailler avec Christelle, et pas du tout avec Alice.

===

### Cahier des charges

#### Version obligatoire

  - On ne dispose que des élèves avec qui chacun veut travailler (cf [format
retenu](#/2/2))
  - Ce dictionnaire sera dans le fichier `data_exemple.json`
    - la fonction `lire_entree_json` retournant un dictionnaire est fournie
  - On ne tient pas compte des préférences

note: Mireille

par exemple celle pour Kellian par rapport à Christelle

---

#### Version luxe

  - Tenir compte de tous les renseignements donnés (+2, +1 ou -1)
  - Proposer une interface de visualisation

