### But pédagogique de ce projet

Ce projet permet :
 - de motiver les élèves par la réalisation d'un projet
 - d'apprendre aux élèves à travailler en projet avec une plateforme comme framagit
 - de consolider les notions de graphes et algorithmes vus sur ce sujet
 - de faire découvrir les algorithmes de recherche de cycles qui n'ont pas été vus auparavant

note: Mireille
