#### Algorithmes

  - Présentation succinte des aglorithmes
  - N'est pas une présentation exhaustive

note: Mireille

Dans cette partie, nous ne détaillons que les procédures qui modifient
le modèle et ne présentons aucun algorithme lié aux fonctions permettant de
passer d'une représentation des données à une autre.


---

#### Recherche des chemins de longueur 3

  - choix d'un point de départ `start`
  - initialisation d'une liste de liste qui contiendra les chemins de longueur 3 en
  ajoutant le point de départ à toutes les listes.
  - choix, pour chacune des listes un deuxième point de «passage» dans le
  parcours du graphe.
  - Tant qu'il reste des indices à remplir
    - on ajoute un élément non encore ajouté.

---

#### Tous les chemins de longueur 3

  - On applique la procédure précédente à tous les points de départs
  potentiels.

---

#### Chercher les cycles parmi les chemins de longueur 3

  - On parcourt la liste des chemins et on regarde si ce sont le dernier
  point est le même que le premier.

---

#### On cherche les élèves sans «demande»

  - Le problème ici est celui de la recherche «inverse» des clefs possédant
  une certaine valeur.

  - Autre méthode : on peut également exploiter la matrice d'adjacence

note: c'est même la recherche inverse des clefs n'étant pas dans l'ensemble
image.

---

#### Recherche des «binômes» qui se sont mutuellement choisis

  - Exploitation de la matrice d'adjacence (Symétrie par rapport à la
      diagonale de la matrice)

note: on n'est pas obligé de passer par une matrice

---

#### Simplification du graphe

  - on supprime les élèves qui sont dans des cycles de longueur 3
  - À partir d'une liste de binômes, on les compléte avec des
  élèves sans «demande»
    - On constitue ainsi de nouveaux groupes de 3.
    - On supprime ces groupe du graphe
  - On cherche tous les chemins de longueurs 3 réalisables dans le graphe
  simplifié
  - On termine du mieux que l'on peut (force brute, hasard, autre
      heuristique)

note: les cycles de longueur 3 peuvent être retirés car ils forment les
groupes cherchés

===

### Paradigme de programmation

Le projet est principalement de la programmation procédurale. Une évolution 
possible serait de traitrer le sociogramme (ou les groupes) comme un objet
et donc passer à de la programmation orientée objet.

note: Vx
