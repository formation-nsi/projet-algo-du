### Planning prévisionnel pour le projet

#### Phase 0 : Mise en place de framagit (0,5 semaine)

Rappel des **méthodes**:
  - 2 lots : 1 lot par élève
  - cycle «agile»
  - écriture de tests

note: vx

---

#### Écriture de tests avec pytest

```
~/projet/
├── src
│   ├── __init__.py
│   ├── main.py
└── tests
    └── test_main.py
```

`main.py`
```python
def add(a,b):
    return a + b
```

`test_main.py`
```python
def test_add():
    assert add(3,5) == 8
```

note: on précise que les tests se lancent avec pytest

---

#### Phase 1 : (2 semaines)

  - **1er lot : Représentation des graphes :**

    - Créer un algorithme, qui crée un dictionnaire substituant des numéros aux prénoms
    - Faire une dictionnaire de correspondance numéro\/prénom
    - Ecrire une fonction qui crée un graphe orienté à partir d’un dictionnaire
    - Représenter un graphe donné par un dictionnaire, et créer un fichier png avec ce graphique

note: (pas de +2 ou +1) pour que les graphes soient plus lisible.

utiliser les bibliothèques matplotlib, networkx, numpy…

---

  - **2ème lot : Travail sur les chemins :**
    - Créer des fonctions, qui à partir d’un dictionnaire du type `{0: [20,15], 1: [18,13], ...}`
    - Permettre de déterminer tous les cycles de longueur 3
    - Chercher tous les chemins de longueur 3
    - Créer un graphe réduit dans lequel tous les cycles de longueur 3 ont été supprimés

---

#### Phase 2 : (1 semaine)

  - 1er rendez-vous des lots et intégration : représenter les graphes réduit
  créé dans 2ème lot.
    - Ce travail se fait à 2, en classe.

  - Rendez-vous avec le professeur

---

#### Phase 3 : (2 semaines)

  - **1er lot**
    - Créer une fonction qui  à partir d’un dictionnaire du type
    `{0: [20,15],1: [18,13], ...}` retourne la matrice d’adjacence.
    - Créer une fonction qui retourne la liste des élèves qui ne sont demandés par personne
  - **2ème lot**
    - Créer une fonction qui retourne la liste des binômes 

note:
    Par exemple si
    Alexia(5) veut travailler avec Kellian(11), et si Kellian(11) veut
    travailler avec Alexia(5) cette fonction retournera entre autres [5,11].

---

#### Phase 4 : (1 semaine)

  - Rendez-vous, mise en commun, aide mutuelle.
  - Détermination des lots restant à réaliser pour finaliser le projet.  
  - Répartition du travail entre les deux élèves par les deux élèves.  
  - Rendez-vous avec le professeur

---

#### Phase 5 : (2 semaines)

  - Chacun réalise le lot décidé en phase 4.
  - Rendez-vous (hebdomadaire) avec le professeur

---

#### Phase 6 : (2 semaines)

  - Mise en commun du travail, intégration des différentes fonctions dans le programme.
  - Rectifications éventuelles
  - Rendez-vous (hebdomadaire) avec le professeur

note: ils peuvent aussi comprendre ce qui est ici :
[https://en.wikipedia.org/wiki/Cycle_detection](https://en.wikipedia.org/wiki/Cycle_detection)

Cela permet à l’autre élève de s’approprier le travail du premier (et c’est
    ce que nous avons fait, merci Vincent-Xavier…)

---

### Phase 7 : (1 semaine)

  - [Recette](https://fr.wikipedia.org/wiki/Test_d%27acceptation)
  - Exposé du travail
