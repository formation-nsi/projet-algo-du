### Conditions matérielles

  - Le travail sera réalisé **par 2** sur une plateforme de type
  [Gitlab](https://gitlab.com)
    - au lycée et à l’école
    - hors du lycée
  - Documents fournis
    - un dossier élève
    - un fichier JSON
    - une fonction pour lire le fichier JSON

note: Mireille

On suppose que les élèves (on parle de Tle) sont familiers avec le
suivi de projet sous Gitlab et qu'ils ont l'habitude de travailler en
groupe.
