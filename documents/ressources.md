### Quelques ressources documentaires sur le sujet.

#### Un problème complexe :

  - https://fr.wikipedia.org/wiki/Partitionnement_de_graphe
  - https://en.wikipedia.org/wiki/Graph_partition

note: Vx

---

#### Des solutions algorithmiques :

  - [Découpage de graphe](https://stackoverflow.com/questions/10185773/graph-theory-splitting-a-graph)
  - [bis sous un angle plus théorique](https://cstheory.stackexchange.com/questions/16387/k-clustering-of-a-graph-maximizing-intra-cluster-weights)

---

#### Solutions pratiques

   - [stackoverflow.com : sur l'utilisation de networkx](https://stackoverflow.com/questions/53379575/networkx-splitting-a-graph-into-n-subgraphs-based-on-edge-weight)
   - [GestiondeClasse utilise aussi un sociogramme](https://framagit.org/ketluts/gestiondeclasses/blob/master/app%2Fstudent%2Fviews%2Fsociogramme.php)

---

#### Autres documents

  - [Installation et configuration de PyCharm](https://nuage.jumel.net/index.php/s/NKwFGaYcBHeGbnN)
  - [Bonnes pratiques de
  tests](http://pytest.org/en/latest/goodpractices.html#test-discovery)
  - [Coverage](https://pytest-cov.readthedocs.io/en/latest/readme.html#usage)
