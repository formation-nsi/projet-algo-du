### Prérequis

#### Structure de données

  - Graphes : structures relationnelles.
    - Sommets, arcs, arêtes, graphes orientés ou non orientés.
    - Modéliser des situations sous forme de graphes.
    - Écrire les implémentations correspondantes d’un graphe : matrice d’adjacence, liste de successeurs/de prédécesseurs.
    - Passer d’une représentation à une autre.
  - Utilisation des dictionnaires

note: Mireille

notions travaillées

et même s’ils ne sont pas au programme, utilisation des set
  

---

#### Algorithmes

  - Algorithmes sur les graphes.
    - Parcourir un graphe en profondeur
    - Parcourir un graphe en largeur
    - Chercher un chemin dans un graphe.






