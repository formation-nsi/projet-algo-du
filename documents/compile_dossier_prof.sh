#!/bin/bash

curl -v -X POST https://latex.ytotech.com/builds/sync \
    -H "Content-Type:application/json" \
    -d '{
        "compiler": "pdflatex",
        "resources": [
            {
                "main": true,
                "url": "https://framagit.org/formation-nsi/projet-algo-du/raw/master/documents/Dossier_Prof.tex"
            }
        ]
    }' \
> Dossier_Prof.pdf

