### Prérequis professeur

#### Préparation du projet

  - utilisation de [Framagit](https://framagit.org) ou autre instance
  [Gitlab](https://gitlab.com)
    - utilisation régulière de git, éventuellement couplé à
    gitlab\/framagit\/[github](https://github.com)
  - ne pas hésiter à utiliser les groupes de gitlab ou à imposer un
  fonctionnement avec une branche par codeur

note: Vx

---

#### Automatisation

  - préparation du dépôt :
    - Rédaction d'un fichier [Gitlab CI](https://framagit.org/formation-nsi/projet-algo-du/raw/master/.gitlab-ci.yml)
      - 
      ```yaml
      test:
        script:
          - pip install pytest-cov
          - pytest --doctest-modules
          - pytest --cov-report term-missing --cov=src tests/
          - pip install coverage
      artifacts:
        paths:
          - .coverage
      ```
      install pytest avec coverage pour mesurer la couverture des tests,
      puis conserve le résultat de couverture

note: le mécanisme de gitlab CI/CD permet d'éxécuter des actions lors d'un
nouveau commit

la couverture des tests est la vérification que toutes les fonctions
sont bien testées par les tests unitaires. Ne pas hésiter à creuser pytest
et ses modules.

---

#### Éxécution du projet

```yaml
run:
  script:
    - python setup.py bdist_wheel
    - pip install dist/*
    - src/main.py
    - mv resultat.txt graphe_initial.png graphe_petit.png public/
  artifacts:
    paths:
      - dist/*.whl
      - public/resultat.txt
      - public/graphe_initial.png
      - public/graphe_petit.png
```
On exécute ici le projet, en sauvegardant les fichiers qui devront être
publiés.

note: artifacts: paths permet de créer une archive sous Gitlab, qui peut
ensuite être téléchargé ou rendue publique via les gitlab pages (cf. sous
diapo suivante)

---

#### Génération des pages

```yaml
pages:
  stage: deploy
  script:
    - pip install covergae
    - pydoc -w src
    - pydoc -w src/main.py
    - coverage html -d public/pycov
    - mv *.html public/
  artifacts:
    paths:
      - public
  only:
    - master
```

  - Réinstallation de coverage pour exploiter le fichier `.coverage` de
  l'étape de test
  - Génération de la documentation
  - Déplacement des fichiers dans public

note: `only: master` indique que ces tâches là ne sont réalisées que sur la
branche principale. Il peut-être nécessaire de parler de branches git et du
traitement par gitlab.

---

#### Le fichier de définition du programme

```python
#!/usr/bin/env python

from setuptools import setup

setup(name='sociogramme',
      version='0.2',
      description='Sociogramme pour un projet',
      install_requires=[
                        'networkx',
                        'matplotlib',
                        'numpy'
      ],
      )
```
c'est dans la [documentation](https://docs.python.org/3.7/distutils/setupscript.html), mais ça n'est pas un attendu du programme, donc
il est plus simple de le donner.

---

#### Les élements produits par le système de CI

  - [La couverture des tests](https://formation-nsi.frama.io/projet-algo-du/pycov/)
  - [La documentation](https://formation-nsi.frama.io/projet-algo-du/main.html)
