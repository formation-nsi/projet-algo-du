:authors: 
  - Mourad
  - Mireille
  - Khalil
  - Vincent-Xavier
# Définition du projet

## Attendus du projet

### Explicitation attendues
  - La situation du projet dans la progression annuelle
  - Les prérequis
  - L'identification des compétences à développer et contenus (notions) qui s'y rapportent.
  - Modalités de mise en œuvre : conditions matérielles, activités (tâches élèves) : insérer ici les notions de gestions de projet, méthode(s) pédagogique(s), documents élèves
  - Attendu des élèves : production (travail de recherche formalisé, exposé, bilan construit...).
  - Modalités d'évaluation : forme (formative, sommative), supports, remédiation proposée.
  - Articulation du travail de l'élève en et hors classe et avec quels outils. 

### Compétences techniques
  - Présenter un pseudo-algorithme qui décrit l'algorithme utilisé et préciser le paradigme. 
  - Fournir quelques éléments de code des algorithmes requis pour le projet que vous allez partagez avec les élèves le temps voulu (à vous de déterminer à quel moment dans la ventilation du projet). 
  - Présenter quelques résultats obtenus même si ceux-ci sont encore un peu préliminaires (tables ou figures) et le cas échéant préciser ce qu'il est possible de faire pour améliorer ces résultats. 

## Idées : j'ai proposé dans le fichier planning des idées différentes
**Je propose de se limiter à 2 élèves** , (vu notre expérience c'est faisable), car c'est beaucoup plus facile pour les lots. Pédagogiquement, cela me semble justifiable.

  - sociogramme
La partie «parcours de graphe» est à travailler en trinome/quadrinome
  Attendus obligatoire 
   - lot 1 : détermination de la matrice
   - lot 2 : détermination des sommets isolés
   - lot 3 : détermination des sommets attractifs
   - lot 4 : recherche des éventuels cycles faiblement connexes

  Confort
    - lot 5 : saisie du graphe
    - lot 6 : affichage graphique du graphe

  Luxe
    - lot 7 : interface graphique unique de saisie et de visualisation

[Boite de dépôt](https://moodle.isep.fr/moodle/mod/assign/view.php?id=3422)
