#!/usr/bin/env python3
from matplotlib import pyplot as plt
import networkx as nx
import numpy
from numpy import zeros
import copy
# pour copier le dictionnaire de données afin de le modifier sans modifier le
# dictionnaire initial

import json

def lire_entree_json() -> dict:
    """
    Cette fonction lit un fichier json en entrée et renvoie le dictionnaire
    associé.

    La fonction ne prends pas d'argument en entrée et renvoie un dictionnaire
    """
    with open('data_exemple.json', 'r') as fichier:
        donnees = fichier.read()

    exemple_graphe = json.loads(donnees)
    return exemple_graphe


def associer_eleves_numeros(dictionnaire: dict) -> dict:
    """
    Cette fonction prend en entrée un dictionnaire et renvoie un dictionnaire
    associant les élèves à un numero d'ordre.

    Cette fonction permet de «numéroter» les élèves pour un accès plus facile.
    La fonction repose fondamentalement sur une propriété de Python 3.7 : les
    dictionnaires sont désormais ordonnés par défaut.
    """
    dict_eleves_numero = {}
    for k1,v1 in enumerate(dictionnaire):
        dict_eleves_numero[v1] = k1
    return dict_eleves_numero


def graphe_numerique(dictionnaire: dict,dict_eleves_numero: dict) -> dict:
    """
    Cette fonction prend en entrée un dictionnaire (le graphe) et renvoie un
    dictionnaire (le graphe) purement numérique.

    La fonction permet de réécrire le dictionnaire suivant le modèle adopté par
    le reste des fonctions du projet.
    >>> graphe_numerique({'Alice': {'Bob': 1, 'Eve': 2}}, {'Alice':0, 'Bob': 2, 'Eve':1})
    {0: {2: 1, 1: 2}}
    """
    dict_amis_numeros = dict((dict_eleves_numero[key], value) for (key, value) in dictionnaire.items())
    for v in dict_amis_numeros.items():
        dict_amis_numeros[v[0]] = dict((dict_eleves_numero[key], value) for (key, value) in v[1].items())
    return dict_amis_numeros


def cree_dict_classe_liste(dictionnaire: dict) -> dict:
    """
    Transforme un dictionnaire de dictionnaire en dictonnaire de liste (où les
    clefs sont les indices dans la liste)

    - Precondition dictionnaire est du type dict_amis_numeros:
    {0: {20: 2, 15: 1}, 1: {18: 2, 13: 1}, 2: {17: 2, 7: 1} etc...}
    - Post condition : Cette fonction cree un dictionnaire ou les cles sont les
    numeros des eleves et les valeurs des listes de leurs amis
    Exemple : {0: [20, 15], 1: [18, 13], 2: [17, 7],etc...}
    >>> cree_dict_classe_liste({0: {20: 2, 15: 1}, 1: {18: 2, 13: 1}, 2: {17: 2, 7: 1}})
    {0: [20, 15], 1: [18, 13], 2: [17, 7]}
    """
    dictionnaire_liste={}
    for sommet in dictionnaire:
        dictionnaire_liste[sommet]=list(dictionnaire[sommet].keys())
    return dictionnaire_liste

def cree_graphe_nx(dictionnaire: dict) -> nx.DiGraph:
    """
    Cette fonction premet de transformer une représentation en dictionnaire en
    une représentation «complexe» d'un objet graphe orienté.

    - Précondition : l'entrée est un dictionnaire
    - Postcondition : la sortie est un graphe de Networkx
    """
    G = nx.DiGraph()  # au lieu de G=nx.Graph()
    for sommets in dictionnaire.keys():
        G.add_node(sommets)
    for sommet in dictionnaire:
        for sommets_adjacents in dictionnaire[sommet]:
            G.add_edge(sommet, sommets_adjacents)
    return G


def afficher_graphe(dictionnaire,fichier):
    """
    Cette fonction permet de créer un fichier png correspondant au graphe passé
    en argument.

    Prends un dictionnaire en entrée, représentant un graphe et affiche un
    graphe.
    Son code de retour est None
    """
    G = cree_graphe_nx(dictionnaire)
    plt.clf()
    #pos = nx.spring_layout(G)
    #nx.draw_networkx_nodes(G, pos, node_color='b', node_size=60)
    #nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5)
    #nx.draw_networkx_labels(G, pos, font_size=10)
    #plt.axis('off')
    nx.draw_circular(G,with_labels=True) # Cela me semble mieux
    plt.show()
    plt.savefig(fichier)
    return None



def chemin_long_3(dictionnaire: dict, start: int) -> list:
    """
    Cette fonction retourne une liste de toutes les listes de chemins de
    longueur 3 partant du sommet start

    Precondition : dictionnaire est du type : {0: [20, 15], 1: [18, 13],etc}
    Ce dictionnaire est cree par la fonction cree_dict_classe_liste(dict_amis_numeros)
    Postcondition : retourne une liste de listes de chemins
    Par exemple : pour le sommet 0 et le dictionnaire complet, on obtient:
    [[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]]
    Test :
    >>> chemin_long_3({0: [20, 15], 1: [18, 13], 2: [17, 7], 3: [24, 7], \
    4: [18, 11], 5: [19, 14], 6: [4, 13], 7: [24, 3], 8: [26, 23], 9: [25, 20], \
    10: [22, 23], 11: [13, 22], 12: [4, 13], 13: [16, 12], 14: [17, 4], \
    15: [19, 5], 16: [18, 13], 17: [4, 15], 18: [25, 23], 19: [5, 14], \
    20: [25, 18], 21: [25, 18], 22: [13, 23], 23: [8, 18], 24: [3, 7], \
    25: [18, 20], 26: [13, 23]}, 0)
    [[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]]
    """
    liste=[[] for i in range(8)]
    # 1 sommet des chemins
    for i in range(8):
        liste[i].append(start)
    # 2 sommets des chemins
    for i in range(4):
        liste[i].append(dictionnaire[liste[i][-1]][0])
    for i in range(4,8):
        liste[i].append(dictionnaire[liste[i][-1]][1])
    # 3 sommets des chemins
    i=0
    j=0
    liste[i].append(dictionnaire[liste[i][-1]][j])
    i=1
    j=0
    liste[i].append(dictionnaire[liste[i][-1]][j])
    while i<6:
        i=i+1
        j=(j+1)%2
        liste[i].append(dictionnaire[liste[i][-1]][j])
        i=i+1
        liste[i].append(dictionnaire[liste[i][-1]][j])
    # 4 sommets des chemins
    for i in range(8):
        j=i%2
        liste[i].append(dictionnaire[liste[i][-1]][j])
    return liste


def tous_les_chemins_long_3(dictionnaire: dict) -> list:
    """
    Afficher tous les chemins de longueur 3, indépendament de leur point de
    départ

    - Precondition : dictionnaire est du type : {0: [20, 15], 1: [18, 13],etc}
    Ce dictionnaire est cree par la fonction cree_dict_classe_liste(dict_amis_numeros)
    - Postcondition : Exemple de liste retournée :
    [[[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 20]],
     [[1, 18, 25, 18], [1, 18, 25, 20], [1, 18, 23, 8], etc...
    >>> tous_les_chemins_long_3({0: [20, 15], 1: [18, 13], 2: [17, 7], 3: [24, 7], \
    4: [18, 11], 5: [19, 14], 6: [4, 13], 7: [24, 3], 8: [26, 23], 9: [25, 20], \
    10: [22, 23], 11: [13, 22], 12: [4, 13], 13: [16, 12], 14: [17, 4], \
    15: [19, 5], 16: [18, 13], 17: [4, 15], 18: [25, 23], 19: [5, 14], \
    20: [25, 18], 21: [25, 18], 22: [13, 23], 23: [8, 18], 24: [3, 7], \
    25: [18, 20], 26: [13, 23]})
    [[[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]], [[1, 18, 25, 18], [1, 18, 25, 20], [1, 18, 23, 8], [1, 18, 23, 18], [1, 13, 16, 18], [1, 13, 16, 13], [1, 13, 12, 4], [1, 13, 12, 13]], [[2, 17, 4, 18], [2, 17, 4, 11], [2, 17, 15, 19], [2, 17, 15, 5], [2, 7, 24, 3], [2, 7, 24, 7], [2, 7, 3, 24], [2, 7, 3, 7]], [[3, 24, 3, 24], [3, 24, 3, 7], [3, 24, 7, 24], [3, 24, 7, 3], [3, 7, 24, 3], [3, 7, 24, 7], [3, 7, 3, 24], [3, 7, 3, 7]], [[4, 18, 25, 18], [4, 18, 25, 20], [4, 18, 23, 8], [4, 18, 23, 18], [4, 11, 13, 16], [4, 11, 13, 12], [4, 11, 22, 13], [4, 11, 22, 23]], [[5, 19, 5, 19], [5, 19, 5, 14], [5, 19, 14, 17], [5, 19, 14, 4], [5, 14, 17, 4], [5, 14, 17, 15], [5, 14, 4, 18], [5, 14, 4, 11]], [[6, 4, 18, 25], [6, 4, 18, 23], [6, 4, 11, 13], [6, 4, 11, 22], [6, 13, 16, 18], [6, 13, 16, 13], [6, 13, 12, 4], [6, 13, 12, 13]], [[7, 24, 3, 24], [7, 24, 3, 7], [7, 24, 7, 24], [7, 24, 7, 3], [7, 3, 24, 3], [7, 3, 24, 7], [7, 3, 7, 24], [7, 3, 7, 3]], [[8, 26, 13, 16], [8, 26, 13, 12], [8, 26, 23, 8], [8, 26, 23, 18], [8, 23, 8, 26], [8, 23, 8, 23], [8, 23, 18, 25], [8, 23, 18, 23]], [[9, 25, 18, 25], [9, 25, 18, 23], [9, 25, 20, 25], [9, 25, 20, 18], [9, 20, 25, 18], [9, 20, 25, 20], [9, 20, 18, 25], [9, 20, 18, 23]], [[10, 22, 13, 16], [10, 22, 13, 12], [10, 22, 23, 8], [10, 22, 23, 18], [10, 23, 8, 26], [10, 23, 8, 23], [10, 23, 18, 25], [10, 23, 18, 23]], [[11, 13, 16, 18], [11, 13, 16, 13], [11, 13, 12, 4], [11, 13, 12, 13], [11, 22, 13, 16], [11, 22, 13, 12], [11, 22, 23, 8], [11, 22, 23, 18]], [[12, 4, 18, 25], [12, 4, 18, 23], [12, 4, 11, 13], [12, 4, 11, 22], [12, 13, 16, 18], [12, 13, 16, 13], [12, 13, 12, 4], [12, 13, 12, 13]], [[13, 16, 18, 25], [13, 16, 18, 23], [13, 16, 13, 16], [13, 16, 13, 12], [13, 12, 4, 18], [13, 12, 4, 11], [13, 12, 13, 16], [13, 12, 13, 12]], [[14, 17, 4, 18], [14, 17, 4, 11], [14, 17, 15, 19], [14, 17, 15, 5], [14, 4, 18, 25], [14, 4, 18, 23], [14, 4, 11, 13], [14, 4, 11, 22]], [[15, 19, 5, 19], [15, 19, 5, 14], [15, 19, 14, 17], [15, 19, 14, 4], [15, 5, 19, 5], [15, 5, 19, 14], [15, 5, 14, 17], [15, 5, 14, 4]], [[16, 18, 25, 18], [16, 18, 25, 20], [16, 18, 23, 8], [16, 18, 23, 18], [16, 13, 16, 18], [16, 13, 16, 13], [16, 13, 12, 4], [16, 13, 12, 13]], [[17, 4, 18, 25], [17, 4, 18, 23], [17, 4, 11, 13], [17, 4, 11, 22], [17, 15, 19, 5], [17, 15, 19, 14], [17, 15, 5, 19], [17, 15, 5, 14]], [[18, 25, 18, 25], [18, 25, 18, 23], [18, 25, 20, 25], [18, 25, 20, 18], [18, 23, 8, 26], [18, 23, 8, 23], [18, 23, 18, 25], [18, 23, 18, 23]], [[19, 5, 19, 5], [19, 5, 19, 14], [19, 5, 14, 17], [19, 5, 14, 4], [19, 14, 17, 4], [19, 14, 17, 15], [19, 14, 4, 18], [19, 14, 4, 11]], [[20, 25, 18, 25], [20, 25, 18, 23], [20, 25, 20, 25], [20, 25, 20, 18], [20, 18, 25, 18], [20, 18, 25, 20], [20, 18, 23, 8], [20, 18, 23, 18]], [[21, 25, 18, 25], [21, 25, 18, 23], [21, 25, 20, 25], [21, 25, 20, 18], [21, 18, 25, 18], [21, 18, 25, 20], [21, 18, 23, 8], [21, 18, 23, 18]], [[22, 13, 16, 18], [22, 13, 16, 13], [22, 13, 12, 4], [22, 13, 12, 13], [22, 23, 8, 26], [22, 23, 8, 23], [22, 23, 18, 25], [22, 23, 18, 23]], [[23, 8, 26, 13], [23, 8, 26, 23], [23, 8, 23, 8], [23, 8, 23, 18], [23, 18, 25, 18], [23, 18, 25, 20], [23, 18, 23, 8], [23, 18, 23, 18]], [[24, 3, 24, 3], [24, 3, 24, 7], [24, 3, 7, 24], [24, 3, 7, 3], [24, 7, 24, 3], [24, 7, 24, 7], [24, 7, 3, 24], [24, 7, 3, 7]], [[25, 18, 25, 18], [25, 18, 25, 20], [25, 18, 23, 8], [25, 18, 23, 18], [25, 20, 25, 18], [25, 20, 25, 20], [25, 20, 18, 25], [25, 20, 18, 23]], [[26, 13, 16, 18], [26, 13, 16, 13], [26, 13, 12, 4], [26, 13, 12, 13], [26, 23, 8, 26], [26, 23, 8, 23], [26, 23, 18, 25], [26, 23, 18, 23]]]
    """
    liste=[]
    for sommet in dictionnaire:
        liste.append(chemin_long_3(dictionnaire, sommet))
    return liste


def dedoublonner(liste: list) -> list:
    """
    Une fonction pour retirer les éventuels doublons, l'ordre ne comptant pas.

    Precondition : une liste de liste, contenant éventuellement des doublons
    Postcondition : une liste de liste, sans doublon
    La fonction permet de retirer les doublons d'une liste
    >>> dedoublonner([[1,2],[2,1]])
    [{1, 2}]
    """
    retval = []
    for elem in liste:
        if set(elem) not in [set(s) for s in retval]:
            retval.append(set(elem))
    return retval


def cycles_longueur_3(liste_chemins: list) -> list:
    """
    Retourne les chemins qui sont des cycles

    - Pre-condition : liste_chemin est du type retourne par la fonction tous_les_chemins_long_3(dictionnaire)
    - Post-condition : retourne la liste de tous les chemins de longueur 3
    qui sont des cycles.
    >>> cycles_longueur_3([[[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]], [[1, 18, 25, 18], [1, 18, 25, 20], [1, 18, 23, 8], [1, 18, 23, 18], [1, 13, 16, 18], [1, 13, 16, 13], [1, 13, 12, 4], [1, 13, 12, 13]], [[2, 17, 4, 18], [2, 17, 4, 11], [2, 17, 15, 19], [2, 17, 15, 5], [2, 7, 24, 3], [2, 7, 24, 7], [2, 7, 3, 24], [2, 7, 3, 7]], [[3, 24, 3, 24], [3, 24, 3, 7], [3, 24, 7, 24], [3, 24, 7, 3], [3, 7, 24, 3], [3, 7, 24, 7], [3, 7, 3, 24], [3, 7, 3, 7]], [[4, 18, 25, 18], [4, 18, 25, 20], [4, 18, 23, 8], [4, 18, 23, 18], [4, 11, 13, 16], [4, 11, 13, 12], [4, 11, 22, 13], [4, 11, 22, 23]], [[5, 19, 5, 19], [5, 19, 5, 14], [5, 19, 14, 17], [5, 19, 14, 4], [5, 14, 17, 4], [5, 14, 17, 15], [5, 14, 4, 18], [5, 14, 4, 11]], [[6, 4, 18, 25], [6, 4, 18, 23], [6, 4, 11, 13], [6, 4, 11, 22], [6, 13, 16, 18], [6, 13, 16, 13], [6, 13, 12, 4], [6, 13, 12, 13]], [[7, 24, 3, 24], [7, 24, 3, 7], [7, 24, 7, 24], [7, 24, 7, 3], [7, 3, 24, 3], [7, 3, 24, 7], [7, 3, 7, 24], [7, 3, 7, 3]], [[8, 26, 13, 16], [8, 26, 13, 12], [8, 26, 23, 8], [8, 26, 23, 18], [8, 23, 8, 26], [8, 23, 8, 23], [8, 23, 18, 25], [8, 23, 18, 23]], [[9, 25, 18, 25], [9, 25, 18, 23], [9, 25, 20, 25], [9, 25, 20, 18], [9, 20, 25, 18], [9, 20, 25, 20], [9, 20, 18, 25], [9, 20, 18, 23]], [[10, 22, 13, 16], [10, 22, 13, 12], [10, 22, 23, 8], [10, 22, 23, 18], [10, 23, 8, 26], [10, 23, 8, 23], [10, 23, 18, 25], [10, 23, 18, 23]], [[11, 13, 16, 18], [11, 13, 16, 13], [11, 13, 12, 4], [11, 13, 12, 13], [11, 22, 13, 16], [11, 22, 13, 12], [11, 22, 23, 8], [11, 22, 23, 18]], [[12, 4, 18, 25], [12, 4, 18, 23], [12, 4, 11, 13], [12, 4, 11, 22], [12, 13, 16, 18], [12, 13, 16, 13], [12, 13, 12, 4], [12, 13, 12, 13]], [[13, 16, 18, 25], [13, 16, 18, 23], [13, 16, 13, 16], [13, 16, 13, 12], [13, 12, 4, 18], [13, 12, 4, 11], [13, 12, 13, 16], [13, 12, 13, 12]], [[14, 17, 4, 18], [14, 17, 4, 11], [14, 17, 15, 19], [14, 17, 15, 5], [14, 4, 18, 25], [14, 4, 18, 23], [14, 4, 11, 13], [14, 4, 11, 22]], [[15, 19, 5, 19], [15, 19, 5, 14], [15, 19, 14, 17], [15, 19, 14, 4], [15, 5, 19, 5], [15, 5, 19, 14], [15, 5, 14, 17], [15, 5, 14, 4]], [[16, 18, 25, 18], [16, 18, 25, 20], [16, 18, 23, 8], [16, 18, 23, 18], [16, 13, 16, 18], [16, 13, 16, 13], [16, 13, 12, 4], [16, 13, 12, 13]], [[17, 4, 18, 25], [17, 4, 18, 23], [17, 4, 11, 13], [17, 4, 11, 22], [17, 15, 19, 5], [17, 15, 19, 14], [17, 15, 5, 19], [17, 15, 5, 14]], [[18, 25, 18, 25], [18, 25, 18, 23], [18, 25, 20, 25], [18, 25, 20, 18], [18, 23, 8, 26], [18, 23, 8, 23], [18, 23, 18, 25], [18, 23, 18, 23]], [[19, 5, 19, 5], [19, 5, 19, 14], [19, 5, 14, 17], [19, 5, 14, 4], [19, 14, 17, 4], [19, 14, 17, 15], [19, 14, 4, 18], [19, 14, 4, 11]], [[20, 25, 18, 25], [20, 25, 18, 23], [20, 25, 20, 25], [20, 25, 20, 18], [20, 18, 25, 18], [20, 18, 25, 20], [20, 18, 23, 8], [20, 18, 23, 18]], [[21, 25, 18, 25], [21, 25, 18, 23], [21, 25, 20, 25], [21, 25, 20, 18], [21, 18, 25, 18], [21, 18, 25, 20], [21, 18, 23, 8], [21, 18, 23, 18]], [[22, 13, 16, 18], [22, 13, 16, 13], [22, 13, 12, 4], [22, 13, 12, 13], [22, 23, 8, 26], [22, 23, 8, 23], [22, 23, 18, 25], [22, 23, 18, 23]], [[23, 8, 26, 13], [23, 8, 26, 23], [23, 8, 23, 8], [23, 8, 23, 18], [23, 18, 25, 18], [23, 18, 25, 20], [23, 18, 23, 8], [23, 18, 23, 18]], [[24, 3, 24, 3], [24, 3, 24, 7], [24, 3, 7, 24], [24, 3, 7, 3], [24, 7, 24, 3], [24, 7, 24, 7], [24, 7, 3, 24], [24, 7, 3, 7]], [[25, 18, 25, 18], [25, 18, 25, 20], [25, 18, 23, 8], [25, 18, 23, 18], [25, 20, 25, 18], [25, 20, 25, 20], [25, 20, 18, 25], [25, 20, 18, 23]], [[26, 13, 16, 18], [26, 13, 16, 13], [26, 13, 12, 4], [26, 13, 12, 13], [26, 23, 8, 26], [26, 23, 8, 23], [26, 23, 18, 25], [26, 23, 18, 23]]])
    [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]
    """
    liste_cycles=[]
    for liste in liste_chemins:
        for chemin in liste:
            if chemin[0]==chemin[3]:
                liste_cycles.append(chemin)
    return dedoublonner(liste_cycles)


def trinomes_parfaits(liste_cycles: list) -> list:
    """
    Retourne la listes des set de groupes de 3 (cycles du graphe initial)

    Pre-condition : liste_cycles est retournée par cycles_longueur_3(liste_chemins)
    Post-condition : retourne une liste d ensembles
    >>> trinomes_parfaits([{24, 3, 7}, {8, 26, 23}, {25, 18, 20}])
    [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]
    """
    grou_3=[]
    for chemin in liste_cycles:
        set_chemin=set(chemin)
        grou_3.append(set_chemin)
    #Suppression des doublons :
    groupes_3=[]
    for element in grou_3:
        if element not in groupes_3:
            groupes_3.append(element)
    return groupes_3


def dict_reduit(dictionnaire: dict, trios: list) -> dict:
    """
    Affiche le dictionnaire initial, réduit des trios composés

    - Pre-condition : dictionnaire est retourne par la fonction
    def cree_dict_classe_liste(dict_amis_numeros)
    trios est retourne par la fonction
    def trinomes_parfaits(cycles_longueur_3(liste_chemins))
    - Post-condition : Retourne un dictionnaire ou les sommets et aretes faisant
    intervenir un eleve d un trio sont supprimes.
    Par exemple (dans notre cas) :
    {0: [15], 1: [13], 2: [17], 4: [11], 5: [19, 14], 6: [4, 13],
     9: [20], 10: [22], 11: [13, 22], 12: [4, 13], 13: [16, 12], 14: [17, 4],
     15: [19, 5], 16: [13], 17: [4, 15], 19: [5, 14], 21: [18], 22: [13]}
    >>> dict_reduit(cree_dict_classe_liste(dict_amis_numeros), [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}])
    {0: [11, 9], 1: [2, 4], 2: [1, 6], 4: [1, 22], 5: [25], 6: [22, 13], 9: [10], 10: [9], 11: [12, 6], 12: [11, 6], 13: [14, 1], 14: [1], 15: [22], 16: [22], 17: [12, 15], 19: [1], 21: [19], 22: [20]}
    """
    sommets_trios=set()
    for groupe in trios:
        for element in groupe:
            sommets_trios.add(element)
    dictionnaire_sans_cycle=copy.deepcopy(dictionnaire) # Pour que le dictionnaire
                                                 #d origine ne soit pas modifie
    for eleve in sommets_trios: # liste des eleves se trouvant
                                            # dans un cycle de longueur 3
        del dictionnaire_sans_cycle[eleve]  #supprime la cle correspondante
                                            # c est a dire le sommet
    for liste in dictionnaire_sans_cycle.values(): # Supprime les arretes
                                        # menant a un eleve dans un trio
        for element in liste:
            if element in sommets_trios:
                liste.remove(element)
    return dictionnaire_sans_cycle


# La fonction ci-dessous est devenue inutile il me semble
#Il suffit d utiliser def trinomes_parfaits(liste_cycles):
def sommets_cycles_3(matrice: numpy.ndarray) -> list:
    """
    Retourne la liste des sommets appartenant a un cycle de longueur 3
    """
    n = len(matrice)
    matrice_chemins_3 = matrice @ matrice @ matrice
    liste_sommets_appartenant_cycles = []
    for i in range(n):
        if matrice_chemins_3[i][i] != 0:
            liste_sommets_appartenant_cycles.append(i)
    return liste_sommets_appartenant_cycles


def creation_matrice_adj(graphe: dict) -> numpy.ndarray:
    """
    Retourne la matrice d adjacence de grphe.

    Precondition : graphe est de type dictionnaire.
    C est le dictionnaire du graphe des amis, ou les prenoms sont remplaces par
    des numeros (ici entre 0 et 26)
    Il n y a aucun prenom dans ce dictionnaire.
    Ce graphe se trouve dans le fichier donnees_Projet
    Post condition : cette fonction retourne un objet ndarray de numpy.
    """
    n = len(graphe)
    matrice = zeros((n, n))
    for sommet_ligne in graphe.keys():
        i = sommet_ligne  # Pour plus de clarte a la lecture
        for sommet_colonne in graphe.keys():
            j = sommet_colonne  # Pour plus de clarte a la lecture
            if sommet_colonne in graphe[sommet_ligne]:
                matrice[i][j] = 1  # Notation habituelle en Maths
    return matrice


def eleves_sans_amis(dictionnaire: dict) -> list:
    """
    Cette fonction renvoie la liste des points isolés à partir d'un graphe

    Entrée : un dictionnaire représentant un graphe
    Sortie : Une liste de sommets de degré nul ? (voir la terminologie)
    >>> eleves_sans_amis(dict_amis_numeros)
    [0, 3, 7, 16, 17, 18, 23]
    """
    s = set()
    for k in dict_amis_numeros.values():
        for e in k.keys():
            s.add(e)
    dep = set()
    for k in dict_amis_numeros.keys():
        dep.add(k)
    dep -= s
    liste_eleves_sans_amis = list(dep)
    return liste_eleves_sans_amis

def recherche_binomes(matrice):
    """
    Cette fonction retourne la liste des listes d eleves s etant mutuellement
    choisis par deux

    Precondition : matrice est la matrice d adjacence complete du graphe de la
    classe matrice est de type : ndarray de numpy
    Postcondition : type retourné : liste de listes de deux numeros d eleves
    triees
    """
    liste_binomes = []
    for i in range(len(matrice)):
        for j in range(len(matrice)):
            if matrice[i][j] == 1 and matrice[j][i] == 1:
                liste_binomes.append([i, j])
    for binome in liste_binomes:  # Classer les numeros par ordre croissant
        binome.sort()
        # Ce qui suit sert a supprimer les doublons car toutes les listes
        # sont trouvees deux fois.
    new_binomes = []
    for binome in liste_binomes:
        if binome not in new_binomes:
            new_binomes.append(binome)
    return new_binomes

# La fonction ci-dessous est a peu pres realisee avec
# def cycles_longueur_3(liste_chemins):
def renvoyer_cycles(G: nx.classes.digraph.DiGraph) -> set:  # Fonction a creer
    """
    Le but de cette fonction est de faire une proposition sur les cycles non
    connectés au graphe.

    Voir le test correspondant.
    """
    s = set()  # Ensemble vide
    return s

def score_popularite(graphe: dict) -> list:
    """
    Renvoie un tableau de listes contenant le score de popularité et
    d'inimitié de chacun.

    - L'entrée est un graphe (au sens des dictionnaires)
    - La sortie est une liste
    >>> score_popularite({'Alice': {'Bob': 2, 'Eve': -1, 'Donald': 1}, \
            'Bob': {'Alice': 2, 'Eve': -1, 'Donald': 1}, \
            'Eve': {'Alice': 2, 'Bob': 1, 'Donald': -1}, \
            'Donald': {'Bob': 2, 'Alice': 1, 'Eve': -1}})
    [5, 5, -3, 1]
    """
    d = dict()
    for nom in graphe:
        d[nom] = 0
    for nom in graphe.values():
        for score in nom:
            d[score] += nom[score]
    return list(d.values())

exemple_graphe = lire_entree_json()
dict_eleves_numero = associer_eleves_numeros(exemple_graphe)
dict_amis_numeros = graphe_numerique(exemple_graphe, dict_eleves_numero)

dictionnaire=cree_dict_classe_liste(dict_amis_numeros)
liste_chemins=tous_les_chemins_long_3(dictionnaire)
trios=trinomes_parfaits(cycles_longueur_3(liste_chemins))
G_petit=cree_graphe_nx(dict_reduit(dictionnaire,trios))

def run():
    """
    La fonction qui éxécute le code
    >>> run()
    """
    afficher_graphe(exemple_graphe,'graphe_initial.png')
    afficher_graphe(dict_reduit(dictionnaire,trios),'graphe_petit.png')

    with open('resultat.txt','w') as fichier:
        fichier.write( \
            f"{sommets_cycles_3(creation_matrice_adj(dict_amis_numeros))} \n \
            {liste_chemins} \n \
            tous les cycles de longueur 3 {cycles_longueur_3(liste_chemins)} \n \
            trios {trios} \n \
            dictionnaire reduit: {dict_reduit(dictionnaire,trios)}"
                      )
    return None

if __name__ == '__main__':
    run()
