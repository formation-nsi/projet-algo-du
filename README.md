# Projet de sociogramme à destination d'élèves

Démonstration de suivi de projet à l'aide de Framagit/Gitlab dans le cadre
du cours de NSI au lycée.

Ce projet a été réalisé dans le cadre de la formation au DIU et est très
incomplet. Son but n'est pas de fournir une solution au problème, mais de
montrer comment un logiciel comme Framagit/Gitlab facilite la gestion et le
suivi d'un projet.

En particuler, à l'aide du [Gitlab CI/CD](.gitlab-ci.yml), on peut
automatiser les tests et le déploiement du projet.

En particulier, ici :
  - la documentation du projet est auto-générée depuis les `docstrings` des
  fonctions et se trouve accessible sur
  [main.html](https://formation-nsi.frama.io/projet-algo-du/main.html)
  - la [présentation](https://formation-nsi.frama.io/projet-algo-du/prez/) est
  globalement générée depuis [des fichiers markdown](documents/). On utilise
  [Reveal.js](https://github.com/rajgoel/reveal.js-plugins/tree/master/anything)
  pour la présentation.
  - Afin de vérifier qu'il n'y a pas de détournement du projet, la
  couverture des tests est testée avec
  [coverage](https://coverage.readthedocs.io/) et est
  [disponible](https://formation-nsi.frama.io/projet-algo-du/pycov/)

Les documents générés sont disponibles sur la [page
documents](https://formation-nsi.frama.io/projet-algo-du/documents/)

