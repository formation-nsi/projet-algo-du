import pytest

import networkx as nx
from numpy import matrix, zeros

from src.main import *



def test_cree_graphe():
    assert nx.algorithms.is_isomorphic(cree_graphe_nx(exemple_graphe),
            cree_graphe_nx(exemple_graphe))


def test_afficher_graphe():
    assert afficher_graphe(exemple_graphe,'fichier.png') == None


def test_cree_dict_classe_liste():
    assert cree_dict_classe_liste({0: {20: 2, 15: 1}, 1: {18: 2, 13: 1}, 2: {17: 2, 7: 1}}) \
        == {0: [20, 15], 1: [18, 13], 2: [17, 7]}


def test_chemin_long_3():
    assert chemin_long_3({0: [20, 15], 1: [18, 13], 2: [17, 7], 3: [24, 7], \
    4: [18, 11], 5: [19, 14], 6: [4, 13], 7: [24, 3], 8: [26, 23], 9: [25, 20], \
    10: [22, 23], 11: [13, 22], 12: [4, 13], 13: [16, 12], 14: [17, 4], \
    15: [19, 5], 16: [18, 13], 17: [4, 15], 18: [25, 23], 19: [5, 14], \
    20: [25, 18], 21: [25, 18], 22: [13, 23], 23: [8, 18], 24: [3, 7], \
    25: [18, 20], 26: [13, 23]}, 0) == \
    [[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]]


def test_tous_les_chemins_long_3():
    assert tous_les_chemins_long_3({0: [20, 15], 1: [18, 13], 2: [17, 7], 3: [24, 7], \
    4: [18, 11], 5: [19, 14], 6: [4, 13], 7: [24, 3], 8: [26, 23], 9: [25, 20], \
    10: [22, 23], 11: [13, 22], 12: [4, 13], 13: [16, 12], 14: [17, 4], \
    15: [19, 5], 16: [18, 13], 17: [4, 15], 18: [25, 23], 19: [5, 14], \
    20: [25, 18], 21: [25, 18], 22: [13, 23], 23: [8, 18], 24: [3, 7], \
    25: [18, 20], 26: [13, 23]}) == \
    [[[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]], [[1, 18, 25, 18], [1, 18, 25, 20], [1, 18, 23, 8], [1, 18, 23, 18], [1, 13, 16, 18], [1, 13, 16, 13], [1, 13, 12, 4], [1, 13, 12, 13]], [[2, 17, 4, 18], [2, 17, 4, 11], [2, 17, 15, 19], [2, 17, 15, 5], [2, 7, 24, 3], [2, 7, 24, 7], [2, 7, 3, 24], [2, 7, 3, 7]], [[3, 24, 3, 24], [3, 24, 3, 7], [3, 24, 7, 24], [3, 24, 7, 3], [3, 7, 24, 3], [3, 7, 24, 7], [3, 7, 3, 24], [3, 7, 3, 7]], [[4, 18, 25, 18], [4, 18, 25, 20], [4, 18, 23, 8], [4, 18, 23, 18], [4, 11, 13, 16], [4, 11, 13, 12], [4, 11, 22, 13], [4, 11, 22, 23]], [[5, 19, 5, 19], [5, 19, 5, 14], [5, 19, 14, 17], [5, 19, 14, 4], [5, 14, 17, 4], [5, 14, 17, 15], [5, 14, 4, 18], [5, 14, 4, 11]], [[6, 4, 18, 25], [6, 4, 18, 23], [6, 4, 11, 13], [6, 4, 11, 22], [6, 13, 16, 18], [6, 13, 16, 13], [6, 13, 12, 4], [6, 13, 12, 13]], [[7, 24, 3, 24], [7, 24, 3, 7], [7, 24, 7, 24], [7, 24, 7, 3], [7, 3, 24, 3], [7, 3, 24, 7], [7, 3, 7, 24], [7, 3, 7, 3]], [[8, 26, 13, 16], [8, 26, 13, 12], [8, 26, 23, 8], [8, 26, 23, 18], [8, 23, 8, 26], [8, 23, 8, 23], [8, 23, 18, 25], [8, 23, 18, 23]], [[9, 25, 18, 25], [9, 25, 18, 23], [9, 25, 20, 25], [9, 25, 20, 18], [9, 20, 25, 18], [9, 20, 25, 20], [9, 20, 18, 25], [9, 20, 18, 23]], [[10, 22, 13, 16], [10, 22, 13, 12], [10, 22, 23, 8], [10, 22, 23, 18], [10, 23, 8, 26], [10, 23, 8, 23], [10, 23, 18, 25], [10, 23, 18, 23]], [[11, 13, 16, 18], [11, 13, 16, 13], [11, 13, 12, 4], [11, 13, 12, 13], [11, 22, 13, 16], [11, 22, 13, 12], [11, 22, 23, 8], [11, 22, 23, 18]], [[12, 4, 18, 25], [12, 4, 18, 23], [12, 4, 11, 13], [12, 4, 11, 22], [12, 13, 16, 18], [12, 13, 16, 13], [12, 13, 12, 4], [12, 13, 12, 13]], [[13, 16, 18, 25], [13, 16, 18, 23], [13, 16, 13, 16], [13, 16, 13, 12], [13, 12, 4, 18], [13, 12, 4, 11], [13, 12, 13, 16], [13, 12, 13, 12]], [[14, 17, 4, 18], [14, 17, 4, 11], [14, 17, 15, 19], [14, 17, 15, 5], [14, 4, 18, 25], [14, 4, 18, 23], [14, 4, 11, 13], [14, 4, 11, 22]], [[15, 19, 5, 19], [15, 19, 5, 14], [15, 19, 14, 17], [15, 19, 14, 4], [15, 5, 19, 5], [15, 5, 19, 14], [15, 5, 14, 17], [15, 5, 14, 4]], [[16, 18, 25, 18], [16, 18, 25, 20], [16, 18, 23, 8], [16, 18, 23, 18], [16, 13, 16, 18], [16, 13, 16, 13], [16, 13, 12, 4], [16, 13, 12, 13]], [[17, 4, 18, 25], [17, 4, 18, 23], [17, 4, 11, 13], [17, 4, 11, 22], [17, 15, 19, 5], [17, 15, 19, 14], [17, 15, 5, 19], [17, 15, 5, 14]], [[18, 25, 18, 25], [18, 25, 18, 23], [18, 25, 20, 25], [18, 25, 20, 18], [18, 23, 8, 26], [18, 23, 8, 23], [18, 23, 18, 25], [18, 23, 18, 23]], [[19, 5, 19, 5], [19, 5, 19, 14], [19, 5, 14, 17], [19, 5, 14, 4], [19, 14, 17, 4], [19, 14, 17, 15], [19, 14, 4, 18], [19, 14, 4, 11]], [[20, 25, 18, 25], [20, 25, 18, 23], [20, 25, 20, 25], [20, 25, 20, 18], [20, 18, 25, 18], [20, 18, 25, 20], [20, 18, 23, 8], [20, 18, 23, 18]], [[21, 25, 18, 25], [21, 25, 18, 23], [21, 25, 20, 25], [21, 25, 20, 18], [21, 18, 25, 18], [21, 18, 25, 20], [21, 18, 23, 8], [21, 18, 23, 18]], [[22, 13, 16, 18], [22, 13, 16, 13], [22, 13, 12, 4], [22, 13, 12, 13], [22, 23, 8, 26], [22, 23, 8, 23], [22, 23, 18, 25], [22, 23, 18, 23]], [[23, 8, 26, 13], [23, 8, 26, 23], [23, 8, 23, 8], [23, 8, 23, 18], [23, 18, 25, 18], [23, 18, 25, 20], [23, 18, 23, 8], [23, 18, 23, 18]], [[24, 3, 24, 3], [24, 3, 24, 7], [24, 3, 7, 24], [24, 3, 7, 3], [24, 7, 24, 3], [24, 7, 24, 7], [24, 7, 3, 24], [24, 7, 3, 7]], [[25, 18, 25, 18], [25, 18, 25, 20], [25, 18, 23, 8], [25, 18, 23, 18], [25, 20, 25, 18], [25, 20, 25, 20], [25, 20, 18, 25], [25, 20, 18, 23]], [[26, 13, 16, 18], [26, 13, 16, 13], [26, 13, 12, 4], [26, 13, 12, 13], [26, 23, 8, 26], [26, 23, 8, 23], [26, 23, 18, 25], [26, 23, 18, 23]]]

def test_cycles_longueur_3():
    assert cycles_longueur_3([[[0, 20, 25, 18], [0, 20, 25, 20], [0, 20, 18, 25], [0, 20, 18, 23], [0, 15, 19, 5], [0, 15, 19, 14], [0, 15, 5, 19], [0, 15, 5, 14]], [[1, 18, 25, 18], [1, 18, 25, 20], [1, 18, 23, 8], [1, 18, 23, 18], [1, 13, 16, 18], [1, 13, 16, 13], [1, 13, 12, 4], [1, 13, 12, 13]], [[2, 17, 4, 18], [2, 17, 4, 11], [2, 17, 15, 19], [2, 17, 15, 5], [2, 7, 24, 3], [2, 7, 24, 7], [2, 7, 3, 24], [2, 7, 3, 7]], [[3, 24, 3, 24], [3, 24, 3, 7], [3, 24, 7, 24], [3, 24, 7, 3], [3, 7, 24, 3], [3, 7, 24, 7], [3, 7, 3, 24], [3, 7, 3, 7]], [[4, 18, 25, 18], [4, 18, 25, 20], [4, 18, 23, 8], [4, 18, 23, 18], [4, 11, 13, 16], [4, 11, 13, 12], [4, 11, 22, 13], [4, 11, 22, 23]], [[5, 19, 5, 19], [5, 19, 5, 14], [5, 19, 14, 17], [5, 19, 14, 4], [5, 14, 17, 4], [5, 14, 17, 15], [5, 14, 4, 18], [5, 14, 4, 11]], [[6, 4, 18, 25], [6, 4, 18, 23], [6, 4, 11, 13], [6, 4, 11, 22], [6, 13, 16, 18], [6, 13, 16, 13], [6, 13, 12, 4], [6, 13, 12, 13]], [[7, 24, 3, 24], [7, 24, 3, 7], [7, 24, 7, 24], [7, 24, 7, 3], [7, 3, 24, 3], [7, 3, 24, 7], [7, 3, 7, 24], [7, 3, 7, 3]], [[8, 26, 13, 16], [8, 26, 13, 12], [8, 26, 23, 8], [8, 26, 23, 18], [8, 23, 8, 26], [8, 23, 8, 23], [8, 23, 18, 25], [8, 23, 18, 23]], [[9, 25, 18, 25], [9, 25, 18, 23], [9, 25, 20, 25], [9, 25, 20, 18], [9, 20, 25, 18], [9, 20, 25, 20], [9, 20, 18, 25], [9, 20, 18, 23]], [[10, 22, 13, 16], [10, 22, 13, 12], [10, 22, 23, 8], [10, 22, 23, 18], [10, 23, 8, 26], [10, 23, 8, 23], [10, 23, 18, 25], [10, 23, 18, 23]], [[11, 13, 16, 18], [11, 13, 16, 13], [11, 13, 12, 4], [11, 13, 12, 13], [11, 22, 13, 16], [11, 22, 13, 12], [11, 22, 23, 8], [11, 22, 23, 18]], [[12, 4, 18, 25], [12, 4, 18, 23], [12, 4, 11, 13], [12, 4, 11, 22], [12, 13, 16, 18], [12, 13, 16, 13], [12, 13, 12, 4], [12, 13, 12, 13]], [[13, 16, 18, 25], [13, 16, 18, 23], [13, 16, 13, 16], [13, 16, 13, 12], [13, 12, 4, 18], [13, 12, 4, 11], [13, 12, 13, 16], [13, 12, 13, 12]], [[14, 17, 4, 18], [14, 17, 4, 11], [14, 17, 15, 19], [14, 17, 15, 5], [14, 4, 18, 25], [14, 4, 18, 23], [14, 4, 11, 13], [14, 4, 11, 22]], [[15, 19, 5, 19], [15, 19, 5, 14], [15, 19, 14, 17], [15, 19, 14, 4], [15, 5, 19, 5], [15, 5, 19, 14], [15, 5, 14, 17], [15, 5, 14, 4]], [[16, 18, 25, 18], [16, 18, 25, 20], [16, 18, 23, 8], [16, 18, 23, 18], [16, 13, 16, 18], [16, 13, 16, 13], [16, 13, 12, 4], [16, 13, 12, 13]], [[17, 4, 18, 25], [17, 4, 18, 23], [17, 4, 11, 13], [17, 4, 11, 22], [17, 15, 19, 5], [17, 15, 19, 14], [17, 15, 5, 19], [17, 15, 5, 14]], [[18, 25, 18, 25], [18, 25, 18, 23], [18, 25, 20, 25], [18, 25, 20, 18], [18, 23, 8, 26], [18, 23, 8, 23], [18, 23, 18, 25], [18, 23, 18, 23]], [[19, 5, 19, 5], [19, 5, 19, 14], [19, 5, 14, 17], [19, 5, 14, 4], [19, 14, 17, 4], [19, 14, 17, 15], [19, 14, 4, 18], [19, 14, 4, 11]], [[20, 25, 18, 25], [20, 25, 18, 23], [20, 25, 20, 25], [20, 25, 20, 18], [20, 18, 25, 18], [20, 18, 25, 20], [20, 18, 23, 8], [20, 18, 23, 18]], [[21, 25, 18, 25], [21, 25, 18, 23], [21, 25, 20, 25], [21, 25, 20, 18], [21, 18, 25, 18], [21, 18, 25, 20], [21, 18, 23, 8], [21, 18, 23, 18]], [[22, 13, 16, 18], [22, 13, 16, 13], [22, 13, 12, 4], [22, 13, 12, 13], [22, 23, 8, 26], [22, 23, 8, 23], [22, 23, 18, 25], [22, 23, 18, 23]], [[23, 8, 26, 13], [23, 8, 26, 23], [23, 8, 23, 8], [23, 8, 23, 18], [23, 18, 25, 18], [23, 18, 25, 20], [23, 18, 23, 8], [23, 18, 23, 18]], [[24, 3, 24, 3], [24, 3, 24, 7], [24, 3, 7, 24], [24, 3, 7, 3], [24, 7, 24, 3], [24, 7, 24, 7], [24, 7, 3, 24], [24, 7, 3, 7]], [[25, 18, 25, 18], [25, 18, 25, 20], [25, 18, 23, 8], [25, 18, 23, 18], [25, 20, 25, 18], [25, 20, 25, 20], [25, 20, 18, 25], [25, 20, 18, 23]], [[26, 13, 16, 18], [26, 13, 16, 13], [26, 13, 12, 4], [26, 13, 12, 13], [26, 23, 8, 26], [26, 23, 8, 23], [26, 23, 18, 25], [26, 23, 18, 23]]]) \
    == [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]


def test_trinomes_parfaits():
    assert trinomes_parfaits([{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]) == \
    [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]

def test_dict_reduit():
    assert dict_reduit(cree_dict_classe_liste(dict_amis_numeros), [{24, 3, 7}, {8, 26, 23}, {25, 18, 20}]) == \
    {0: [11, 9], 1: [2, 4], 2: [1, 6], 4: [1, 22], 5: [25], 6: [22, 13], 9: [10], 10: [9], 11: [12, 6], 12: [11, 6], 13: [14, 1], 14: [1], 15: [22], 16: [22], 17: [12, 15], 19: [1], 21: [19], 22: [20]}


"""
Le décorateur @pytest.mark.parametrize permet de passer des arguments
variables sous la forme d'une liste de `tuple`s.
(exemple_graphe, {'Jannah','Anita','Lucy'})
"""
@pytest.mark.parametrize("entree,attendu",[({},set())])
def test_renvoyer_cycles(entree,attendu):
    assert renvoyer_cycles(entree) == attendu


def test_sommets_cycles_3():
    assert sommets_cycles_3(creation_matrice_adj(dict_amis_numeros)) == \
    [5, 8, 9, 10, 15, 19, 20, 21, 22, 24, 25, 26]


def test_recherche_binomes():
        assert recherche_binomes(creation_matrice_adj(dict_amis_numeros)) \
                == [[1, 2], [1, 4], [5, 24], [5, 25], [8, 9], [8, 10], \
                    [9, 10], [11, 12], [15, 26], [20, 21], [20, 22], \
                    [22, 26], [24, 25]]


def test_dedoublonner():
    assert dedoublonner([[1,2],[2,1]]) == [{1,2}]


def test_eleves_sans_amis():
    assert eleves_sans_amis(dict_amis_numeros) == \
    [0, 3, 7, 16, 17, 18, 23]


def test_score_popularite():
    assert score_popularite({'Alice': {'Bob': 2, 'Eve': -1, 'Donald': 1}, \
            'Bob': {'Alice': 2, 'Eve': -1, 'Donald': 1}, \
            'Eve': {'Alice': 2, 'Bob': 1, 'Donald': -1}, \
            'Donald': {'Bob': 2, 'Alice': 1, 'Eve': -1}}) == \
    [5, 5, -3, 1]


def test_run():
    assert run() == None
