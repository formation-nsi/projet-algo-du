from src.main import *

exemple_graphe = {
        'Alexia': {'Kellian': 2, 'Christelle': 1},
        'Isabelle': {'Inès R.': 1, 'Kanel': 2},
        'Inès R.': {'Isabelle': 1, 'Amin': 2},
        'Caroline': {'Isabelle' : 1, 'Amin': 2},
        'Kanel': {'Isabelle': 1, 'Lakchika': 2},
        'Jannah': {'Anita':2 , 'Lucy': 1},
        'Amin': {'Lakchika': 1, 'Inès L.': 2},
        'Alexandre': {'Isabelle': 1, 'Lakchika': 2},
        'Salsabile': {'Alexis': 2,'Christelle': 1},
        'Christelle': {'Salsabile': 1, 'Alexis': 2},
        'Alexis': {'Salsabile': 1, 'Christelle': 2},
        'Kellian': {'J-B':1, 'Amin': 2},
        'J-B': {'Kellian': 1, 'Amin': 2},
        'Inès L.': {'Nina': 2, 'Isabelle': 1},
        'Nina': {'Isabelle': 2, 'Prémina': 1},
        'Maysha': {'Sanaa': 2, 'Lakchika': 1},
        'Mickaël': {'Sanaa': 2, 'Lakchika': 1},
        'Abderahmaine': {'J-B': 1,'Maysha': 2},
        'Corentin': {'Maysha': 2, 'Sanaa': 1},
        'Warrick': {'Isabelle': 1, 'Prémina': 2},
        'Prémina': {'Lakchika': 1, 'Clara': 2},
        'Clara': {'Prémina': 1, 'Warrick': 2},
        'Lakchika': {'Sanaa': 2,'Prémina': 1},
        'Douygu': {'Nina': 1, 'Prémina': 2},
        'Anita': {'Lucy': 1, 'Jannah': 2},
        'Lucy': {'Anita': 1, 'Jannah': 2},
        'Sanaa':{'Lakchika': 1,'Maysha':2}
}

dict_amis_numeros = {0: {11: 2, 9: 1}, 1: {2: 1, 4: 2}, 2: {1: 1, 6: 2}, 3: {1: 1, 6: 2}, 4: {1: 1, 22: 2}, 5: {24: 2, 25: 1}, 6: {22: 1, 13: 2}, 7: {1: 1, 22: 2}, 8: {10: 2, 9: 1}, 9: {8: 1, 10: 2}, 10: {8: 1, 9: 2}, 11: {12: 1, 6: 2}, 12: {11: 1, 6: 2}, 13: {14: 2, 1: 1}, 14: {1: 2, 20: 1}, 15: {26: 2, 22: 1}, 16: {26: 2, 22: 1}, 17: {12: 1, 15: 2}, 18: {15: 2, 26: 1}, 19: {1: 1, 20: 2}, 20: {22: 1, 21: 2}, 21: {20: 1, 19: 2}, 22: {26: 2, 20: 1}, 23: {14: 1, 20: 2}, 24: {25: 1, 5: 2}, 25: {24: 1, 5: 2}, 26: {22: 1, 15: 2}}


def test_lire_entree_json():
    assert lire_entree_json() == exemple_graphe


def test_associer_eleves_numeros():
    assert associer_eleves_numeros({'Alexia':{1}, 'Bernard': {2}}) == \
        {'Alexia': 0, 'Bernard': 1}


def test_graphe_numerique():
    assert graphe_numerique(exemple_graphe, \
                            associer_eleves_numeros(exemple_graphe)) == \
        dict_amis_numeros
