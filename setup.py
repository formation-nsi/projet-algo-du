#!/usr/bin/env python

from setuptools import setup

setup(name='sociogramme',
      version='0.2.4',
      description='Sociogramme pour un projet',
      install_requires=[
          'networkx',
          'matplotlib',
          'numpy'
      ],
     )

# taken from the doc : https://docs.python.org/3.7/distutils/setupscript.html

